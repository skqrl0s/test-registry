FROM php:7.1-apache

EXPOSE 80

WORKDIR /app

ENV NODE_VERSION=9.7.1

RUN apt-get update -q && \
    apt-get install --no-install-recommends -qy curl git zip unzip wget zlib1g-dev && \
    curl -sS https://getcomposer.org/installer |php -- --install-dir=/usr/local/bin --filename=composer && \
    curl -L -o /tmp/nodejs.tar.gz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz && \
    tar xfvz /tmp/nodejs.tar.gz -C /usr/local --strip-components=1 && \
    rm -f /tmp/nodejs.tar.gz && \
    npm install yarn -g && \
    apt-get clean && \
    rm -r /var/lib/apt/lists/*

RUN usermod -u 1000 www-data
RUN apt-get update -q
RUN apt-get install -y libpq-dev && \
    docker-php-ext-configure mysqli -with-mysql=/usr/local/mysql && \
    docker-php-ext-install pdo pdo_mysql opcache zip


COPY docker/php.ini /etc/php/7.1/cli/conf.d/50-setting.ini
COPY docker/httpd.conf /etc/apache2/apache2.conf
