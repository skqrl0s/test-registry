ENV?=prod
DOCKER_IMAGE=app
APP=docker exec $(DOCKER_IMAGE)
EXEC=docker-compose exec app
RUN=docker-compose run --rm $(DOCKER_IMAGE)
CONSOLE=$(RUN) bin/console

.DEFAULT_GOAL := help
.PHONY: help start stop clear clean deps composer yarn db

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

##
## Project setup
##---------------------------------------------------------------------------
start:          ## Install and start the project containers
start:
	docker-compose up -d

stop:           ## Stop the project containers
stop:
	docker-compose down

clear:            ## Remove all the cache, the logs, the sessions and the built assets
clear:
	-$(EXEC) rm -rf var/cache/*
	-$(EXEC) rm -rf var/sessions/*
	-$(EXEC) rm -rf supervisord.log supervisord.pid npm-debug.log .tmp
	-$(EXEC) $(CONSOLE) redis:flushall -n
	rm -rf var/logs/*
	rm -rf web/built
	rm var/.php_cs.cache

clean:            ## Clear and remove dependencies
clean: clear
	rm -rf vendor node_modules

##
## Assets
##---------------------------------------------------------------------------
deps:           ## Install all dependencies
deps: composer yarn build-assets

composer:       ## Install composer dependencies
composer:
	$(RUN) composer install

yarn:           ## Install yarn dependencies
yarn:
	$(RUN) yarn install

build-assets:	## Build front-end assets
build-assets:
	$(RUN) yarn run encore dev

##
## Database
##---------------------------------------------------------------------------
db:             ## Setup database and generate data for dev purpose
db:
	$(CONSOLE) doctrine:database:drop --force --if-exists
	$(CONSOLE) doctrine:database:create --if-not-exists
	$(CONSOLE) doctrine:schema:update --force
	$(CONSOLE) doctrine:fixtures:load -n

