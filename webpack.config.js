var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('web/dist/')
    .setPublicPath('/dist')
    .addEntry('app', './assets/js/app.js')
    .enableSassLoader(function (sassOptions) {}, {
        resolveUrlLoader: false
    })
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
;

module.exports = Encore.getWebpackConfig();